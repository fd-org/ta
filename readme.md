## Docker commands for image configuration

* Download existing image from Docker HUB

```
docker pull footballdata/ta
```

* Start container (Standalone without compose)

``` 
docker run -d --name ta -p 4444:4444 -p 5900:5900 -v C:/Users/abn14/Git/TA:/dev/shm footballdata/ta/
``` 
  
## To build image: 

```
docker build --no-cache -t footballdata/ta .
```

## To run Docker compose (Site and Test images):

* Navigate to Docker folder:

```
cd Docker
```

 * To run images PROD version:
 
```
docker-compose -f docker-compose-prod.yml up -d
```

* To run images for Testing purposes:

```
docker-compose up -d
``` 

## Navigate to the Site

* http://proxy

## To Run Tests:

1.Connect to image:

```
docker exec -it docker_ta_1 bash 
```

2.Navigate to src folder:

```
cd /dev/shm/ta/src 
```

3.Run test

```
python3 -m pytest --html=result/test_report/report.html
```

4.To see test execution flow in realtime:

* Install "Remote desktop viewer for VNC server
* Connect via Remote desktop to image: `localhost:5900`
* Default Password: `secret` 