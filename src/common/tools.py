import os
import time
from src.common.constant import PytestOption


def create_file_in_log_directory(file_name, file_type, sub_path='', scope='function'):
    assert scope in ['function', 'module', 'session']
    rounding_file_name = f'{file_name}_{str(round(time.time()))}.{file_type}'
    test_name_split = os.environ.get('PYTEST_CURRENT_TEST').split(' ')[0].split('::')
    if len(test_name_split) == 3 and scope == 'function':
        artifact_directory = PytestOption.ARTIFACT_DIRECTORY + f'/{test_name_split[1]}/{test_name_split[2]}'
    elif scope != 'session':
        artifact_directory = PytestOption.ARTIFACT_DIRECTORY + f'/{test_name_split[1]}'
    else:
        artifact_directory = PytestOption.ARTIFACT_DIRECTORY
    current_directory = os.path.abspath('.')
    destination_directory = os.path.join(current_directory, artifact_directory, sub_path)
    try:
        os.makedirs(destination_directory)
    except FileExistsError:
        if os.path.exists(os.path.join(destination_directory, rounding_file_name)):
            i = 1
            while True:
                rounding_file_name = f'{rounding_file_name.split(".")[0]}_({i}).{file_type}'
                if os.path.exists(os.path.join(destination_directory, rounding_file_name)):
                    i += 1
                else:
                    break
    return os.path.join(destination_directory, rounding_file_name)