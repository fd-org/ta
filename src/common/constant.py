class PytestOption(object):
    ARTIFACT_FOLDER_NAME = 'artifact'
    ARTIFACT_DIRECTORY = 'result/test_report/' + ARTIFACT_FOLDER_NAME


class FootballSite(object):
    MAIN_PAGE = 'http://proxy/'
    FOOTBALL_DATA_ORG = "https://www.football-data.org/"