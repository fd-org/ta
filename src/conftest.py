import pytest
from selenium import webdriver
from selenium.common.exceptions import WebDriverException, SessionNotCreatedException
from src.common.tools import create_file_in_log_directory
from src.common.constant import PytestOption


driver = None


def pytest_addoption(parser):
    parser.addoption('--logsymlink', type='string', default='logs',
                     help='Set where to create symlink to log dir')
    parser.addoption(
        "--admin_path", action="store", default="", type="string", help="url to visit ADMIN.html"
    )
    parser.addoption(
        "--head", action="store_true", help="set to true if you want chrome to be displayed"
    )
    parser.addoption(
        "--dev_tool", action="store_true", help="set to true if you want dev tool to be opened by default"
    )


@pytest.mark.hookwrapper
def pytest_runtest_makereport(item):
    """
    Extends the PyTest Plugin to take and embed screenshot in html report, whenever test fails.
    :param item:
    """
    pytest_html = item.config.pluginmanager.getplugin('html')
    outcome = yield
    report = outcome.get_result()
    extra = getattr(report, 'extra', [])

    if report.when == 'call' or report.when == "setup":
        xfail = hasattr(report, 'wasxfail')
        if (report.skipped and xfail) or (report.failed and not xfail):
            destination_file = create_file_in_log_directory('FAIL', 'png')
            try:
                browser().save_screenshot(destination_file)
            except (WebDriverException, SessionNotCreatedException, AttributeError) as e:
                print(f'WARNING: Cannot save screenshot, because browser is not running.\n{str(e)}')
            html = '<div><img src="%s" alt="screenshot" style="width:304px;height:228px;" ' \
                   'onclick="window.open(this.src)" align="right"/></div>' % destination_file[destination_file.find(PytestOption.ARTIFACT_FOLDER_NAME):]
            extra.append(pytest_html.extras.html(html))
        report.extra = extra


def browser():
    global driver
    return driver


def init_browser():
    global driver

    if driver is not None:
        print('WARNING: Driver was supposed to be None at the start of test, but was not.')
        _close_browser()

    chrome_options = webdriver.ChromeOptions()

    chrome_options.add_argument("start-maximized")
    chrome_options.add_argument("disable-infobars")
    chrome_options.add_argument("--disable-extensions")
    chrome_options.add_argument("--disable-gpu")
    chrome_options.add_argument("--no-sandbox")

    driver = webdriver.Chrome(options=chrome_options)


def _close_browser():
    global driver
    if driver is not None:
        try:
            driver.quit()
        except BaseException:
            print('WARNING: Browser was not None and quit() failed. Resorting to process termination.')
        finally:
            driver = None


def _setup_actions():
    destination_file = create_file_in_log_directory('chrome_logs', 'log', scope='module')
    init_browser()
    return destination_file


def _teardown_actions(destination_file):
    with open(destination_file, 'w') as console_log:
        try:
            log = browser().get_log('browser')
        except WebDriverException as e:
            print(f'WARNING: Exception when getting browser log:\n{str(e)}')
            _close_browser()
            return

        console_log.writelines(["%s\n" % entry for entry in log])

    _close_browser()


@pytest.fixture(scope='function')
def init_test():
    destination_file = _setup_actions()

    yield

    _teardown_actions(destination_file)