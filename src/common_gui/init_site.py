from src.common.constant import FootballSite
from src.conftest import browser
from selenium.common.exceptions import WebDriverException


def navigate_to_the_site():
    try:
        browser().get(FootballSite.MAIN_PAGE)
    except WebDriverException as e:
        print(f"ERROR: Exception when getting Footbal site: \n{str(e)}")
        raise e

