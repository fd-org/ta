from src.common_gui.page_elements.page_elements import PageElement


class NavigationPanel(PageElement):

    def __init__(self):
        super().__init__("//app-root/app-header/nav")
        self.football_icon = PageElement(f"{self.xpath}//img[@alt='Company Logo']")
