from time import sleep

from selenium.common.exceptions import NoSuchElementException, ElementNotVisibleException, TimeoutException, \
    WebDriverException, StaleElementReferenceException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.select import Select
from src.conftest import browser



class PageElement:
    def __init__(self, xpath):
        self.driver = browser()
        self.xpath = xpath

    def is_present(self):
        try:
            self.driver.find_element(By.XPATH, self.xpath)
            return True
        except NoSuchElementException:
            return False

    def wait_until_present(self, timeout=10.0):
        try:
            WebDriverWait(self.driver, timeout).until(
                expected_conditions.presence_of_element_located((By.XPATH, self.xpath))
            )
            return True
        except TimeoutException:
            return False

    def should_be_present(self, timeout=10.0):
        if not self.wait_until_present(timeout):
            raise NoSuchElementException(f'There is no element after {timeout} seconds: {self.xpath}')

    def wait_until_not_present(self, timeout=10.0):
        try:
            WebDriverWait(self.driver, timeout).until_not(
                expected_conditions.presence_of_element_located((By.XPATH, self.xpath))
            )
            return True
        except TimeoutException:
            return False

    def should_not_be_present(self, timeout=10.0):
        if not self.wait_until_not_present(timeout):
            raise WebDriverException(f'Page still contained element after {timeout} seconds: {self.xpath}')

    def is_visible(self):
        try:
            self.driver.find_element(By.XPATH, self.xpath).is_displayed()
            return True
        except NoSuchElementException or ElementNotVisibleException:
            return False

    def should_be_visible(self, timeout=10.0):
        if not self.wait_until_visible(timeout):
            raise ElementNotVisibleException(
                f'Element was supposed to be visible in {timeout} seconds but was not: {self.xpath}')

    def wait_until_visible(self, timeout=10.0):
        try:
            WebDriverWait(self.driver, timeout).until(
                expected_conditions.visibility_of_element_located((By.XPATH, self.xpath))
            )
            return True
        except TimeoutException:
            return False

    def click(self):
        try:
            if self.wait_until_visible():
                self.driver.find_element(By.XPATH, self.xpath).click()
            else:
                raise ElementNotVisibleException(f'Element cannot be clicked, because it was not visible: {self.xpath}')
        except WebDriverException:
            raise WebDriverException(f'Element cannot be clicked: {self.xpath}')

    def get_text(self):
        if self.wait_until_present():
            return self.driver.find_element(By.XPATH, self.xpath).text
        else:
            raise TimeoutException(f'Cannot get element text: {self.xpath}')

    def get_attribute(self, attribute):
        for _ in range(0, 4):
            if self.wait_until_present():
                try:
                    return self.driver.find_element(By.XPATH, self.xpath).get_attribute(attribute)
                except StaleElementReferenceException:
                    sleep(0.25)
        raise TimeoutException(f'Cannot get element attribute {attribute}: {self.xpath}')

    def send_keys(self, data):
        if self.wait_until_visible():
            self.driver.find_element(By.XPATH, self.xpath).send_keys(data)
        else:
            raise TimeoutException(f'Cannot send keys to element: {self.xpath}')

    def clear(self):
        if self.wait_until_visible():
            self.driver.find_element(By.XPATH, self.xpath).clear()
        else:
            raise TimeoutException(f'Cannot clear element: {self.xpath}')

    def should_be_enabled(self):
        PageElement(f'{self.xpath}[not(@disabled)]').should_be_visible()

    def should_be_disabled(self):
        PageElement(f'{self.xpath}[@disabled]').should_be_visible()

    def get_css_property(self, property):
        if self.wait_until_present():
            return self.driver.find_element(By.XPATH, self.xpath).value_of_css_property(property)
        else:
            raise TimeoutException(f'Cannot get element CSS property {property}: {self.xpath}')

    def select_by_text(self, visible_text):
        try:
            Select(self.driver.find_element(By.XPATH, self.xpath)).select_by_visible_text(visible_text)
            return True
        except ElementNotVisibleException:
            return False

    def get_current_page_url(self):
        return self.driver.current_url
