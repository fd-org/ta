import src.common_gui.init_site as init_site
from src.common_gui.page_elements.common.nav_panel import NavigationPanel
from src.common.constant import FootballSite


def test_main_icon_leads_to_external_page():
    init_site.navigate_to_the_site()
    current_step = NavigationPanel()
    current_step.football_icon.click()
    assert current_step.get_current_page_url() == FootballSite.FOOTBALL_DATA_ORG
